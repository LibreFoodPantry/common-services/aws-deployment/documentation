
# Deploy an Empty Cluster
This is a step by step description of setting up an empty cluster as described in our issues during our Sprint.

The issues can be found here:
https://gitlab.com/LibreFoodPantry/common-services/aws-deployment/general/-/issues/27
https://gitlab.com/LibreFoodPantry/common-services/aws-deployment/general/-/issues/34

## Number of Nodes and Pods
The frontend contains two images, and the front end contains three.

Frontend-main contains: “backend-server” and “backend-database”.
Backend-main contains: “backend-server”, “backend-database”, and “test-runner”.

This can be seen in their respective docker-compose.yaml files.

They should each be hosted on their own node. Frontend-main should have two pods, and backend-main should contain three pods.

## EKS and VPC Creation
These are the steps to create a Cluster and its VPC.

### Configure Cluster:
- Kubernetes Version should be the default
- Cluster Service Role should be “EKS_Cluster_Manager”

### Specify Networking:
We need to create a functional VPC. To do this, go to “Create VPC”.
(At the time of writing this, I’ve already created one. It is simply named “vpc”)
- Select “VPC, subnets, etc.”
- Select Auto-generate name
- Make sure the IPv4 CIDR block is 10.0.0.0/16
- No IPv6 CIDR block
- Tenancy is default
- Availability Zones is set to 2
- Number of public subnets is 2
- Number of private subnets is 2
- NAT gateway set to “In 1 AZ”
- VPC endpoints is “S3 Gateway”
- Make sure to enable “DNS hostnames” and “DNS resolution” 

- Select the default security group
- Select “Public and Private” cluster endpoint access
- Leave the networking add-ons to the default

### Configure Logging:
- Enable All Control Plane Logging options

### Review and create:
- Select “Create”

## Creating Nodes: 
Once your cluster is created select it, then go to Configurations -> Node Groups -> Add Node Group
Since we need two nodes, we need two node groups. One should be for the frontend example, the other for the backend example.

### Frontend nodegroup:
- Set name to frontend
- Set IAM Role to “Testing_EC2_Node_Role”
- Set Kubernetes labels to have a key-pair value of “Name-Frontend”

- Set compute and scaling configuration:
- Set AMI type to Amazon Linux 2
- Set instance types to only have: “t3.small”
- Set Node Group scaling confirmation to have a maximum size of 1 node, maximum size of 2 nodes, and a desired size of 1 node

### Backend nodegroup:
- Set name to backend
- Set IAM Role to “Testing_EC2_Node_Role”
- Set Kubernetes labels to have a key-pair value of “Name-Backend”

- Set compute and scaling configuration:
- Set AMI type to Amazon Linux 2
- Set instance types to only have: “t3.small”
- Set Node Group scaling confirmation to have a maximum size of 1 node, maximum size of 2 nodes, and a desired size of 1 node

## Problems
There is a small problem relating to the node groups that I can’t figure out. Each node group will have a health error named “Ec2SubnetinvalidConfiguration”. I think there is an option that we have to set that I’m not certain about.

## Context:
Availability Zones must be set to 2 for higher availability. When a node / pod scales horizontally, it can scale into another availability zone, known as Region bounded Auto Scaling Groups. Availability Zone bounded Auto Scaling Groups would scale vertically in each of their respective zones, which is probably better.

Pods nodes and pods will have to be set using something called "eksctl". When we create a cluster locally, we can send the processes over to the EKS cluster.



